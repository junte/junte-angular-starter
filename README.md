# JunteAngularStarter

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.6.

## Create junte-angular library - instructions

* upgrade library version (in `/projects/junte-angular/package.json`)
* npm run package
* transfer README.md file to library (cp `projects/junte-angular/README.md` `dist/junte-angular/README.md`)
* npm login (autorization to NPM)
* npm run publish (before use< pls change version of file in package-json, for example `junte-angular-0.0.3.tgz`)

## Project file structure

* assets
  * fonts (text fonts and svg icons fonts)
  * i18n/[lang].json ([lang] is the lang that you're using, for english it could be en)
  * icons (SVG icons)
  * images (raster images for layouts)
  * mocks (rendering by gulp command)
  * styles (CSS, SCSS styles)
* components
  * [folder_name]/[component_name].ts
  * shared/[folder_name]/[component_name].ts
* decorators
* diagnostics
* directives
* environments
* guards
* injects
* managers
* mocks
  * helpers
  * images
  * objects
  * services
* models
  * [model_name].ts
  * errors/[model_name].ts
  * signals/[model_name].ts
* pipes
* services
* utils

## Code scaffolding - Junte Schematic

Run `ng g component component-name` to generate a new component. 

You can also use `ng g directive | pipe | schema | class | guard | interface | enum| module`.

For run schematics commands need local or global library `npm install -g @angular-devkit/schematics-cli`

For using schema run `cd schema`, `npm install`, `npm run build`, `sudo npm link`, `cd ..`, `npm link schema`
```
Create module: `npx gulp componentModule --name=test --r` (--routing is optional)

Create component: `npx gulp component --name=test`

Create service: `npx gulp service --name=test`

Create model: `npx gulp model --name=test`

Create mock helper: `npx gulp mockHelper --name=test`

Create mock object: `npx gulp mockObject --name=test`
