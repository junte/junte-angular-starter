import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateCompiler, TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {TranslateMessageFormatCompiler} from 'ngx-translate-messageformat-compiler';
import {TestServiceProvider} from 'services/test/test.provider';
import {ErrorModule} from 'components/shared/error/module';
import {HttpService} from '../../../projects/junte-angular/src/services/http.service';
import {HttpMockService} from '../../../projects/junte-angular/src/services/http-mock.service';
import {Config} from '../../../projects/junte-angular/src/config';
import {AppConfig} from '../../app-config';
import {SignalsService} from '../../../projects/junte-angular/src/services/signals.service';
import {AuthorizationGuard} from 'guards/authorization.guard';
import {MeManager} from '../../managers/me.manager';
import {MeServiceProvider} from '../../services/me/me.provider';

export function translateLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json?' + Math.random());
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ErrorModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (translateLoaderFactory),
        deps: [HttpClient]
      },
      compiler: {
        provide: TranslateCompiler,
        useClass: TranslateMessageFormatCompiler
      }
    }),
  ],
  providers: [
    {
      provide: Config,
      useClass: AppConfig
    },
    AppConfig,
    HttpClient,
    HttpService,
    HttpMockService,
    TestServiceProvider,
    SignalsService,
    AuthorizationGuard,
    MeManager,
    MeServiceProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
