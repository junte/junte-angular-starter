import {Component, Inject, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {EN_LANG, PLATFORM_DELAY} from 'environments/consts';
import {combineLatest} from 'rxjs';
import {map, delay, filter} from 'rxjs/operators';
import {TestService} from 'services/test/test.service';
import {test_service} from 'services/test/test.interface';
import {HttpService} from '../../../projects/junte-angular/src/services/http.service';
import {HttpMockService} from '../../../projects/junte-angular/src/services/http-mock.service';
import {Animal} from '../../models/animal';
import {AppConfig} from '../../app-config';
import {Config} from '../../../projects/junte-angular/src/config';
import {SignalsService} from '../../../projects/junte-angular/src/services/signals.service';
import {AnimalChangedSignal} from '../../models/signals/animal';
import {deserialize} from 'serialize-ts';
import {LoggingLevel} from '../../diagnostics/loggin-level';
import {Logger} from '../../diagnostics/logger';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  private logger = new Logger('@AppComponent', LoggingLevel.Debug);
  loading = false;
  animal: Animal;
  error: Error;

  constructor(@Inject(test_service) private testService: TestService,
              @Inject(Config) public config: AppConfig,
              private http: HttpService,
              private httpMock: HttpMockService,
              private translate: TranslateService,
              private signalsService: SignalsService) {
    translate.setDefaultLang(EN_LANG);
    translate.use(EN_LANG);

    config.language = !!localStorage.language ? localStorage.language : EN_LANG;
  }

  ngOnInit() {
    combineLatest(this.http.requests$, this.httpMock.requests$)
      .pipe(map(([requests1, requests2]) => requests1 + requests2))
      .subscribe(requests => this.loading = requests > 0);

    this.testService.get()
      .pipe(delay(PLATFORM_DELAY))
      .subscribe(animal => {
          this.logger.debug('testService works');
          this.animal = animal;
        },
        error => this.error = error);

    this.signalsService.signals$
      .pipe(filter(signal => signal instanceof AnimalChangedSignal))
      .subscribe(() => this.animal = deserialize({ animal_type: 'cat', name: 'Musy' } , Animal));
  }

  pushSignal() {
    this.signalsService.signal(new AnimalChangedSignal());
  }
}
