import { Component, Inject } from '@angular/core';
import { Error } from '../../../projects/junte-angular/src/models/errors';
import { AppConfig } from '../../app-config';
import { Router } from '@angular/router';
import { IUsersService, users_service } from '../../services/users/users.interface';
import { LoginCredentials } from '../../models/login-credentials';
import { delay, finalize } from 'rxjs/operators';
import { PLATFORM_DELAY } from '../../environments/consts';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { validate } from '../../../projects/junte-angular/src/utils/form';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.template.html'
})

export class SigninComponent {

  loading: boolean;
  error: Error;

  loginForm: FormGroup = this.builder.group({
    login: [null, [Validators.required]],
    password: [null, [Validators.required]]
  });

  constructor(@Inject(users_service) protected usersService: IUsersService,
              private builder: FormBuilder,
              private config: AppConfig,
              private router: Router) {
  }

  login() {
    if (validate(this.loginForm)) {
      this.loading = true;
      this.usersService.login(this.loginForm.value as LoginCredentials)
        .pipe(delay(PLATFORM_DELAY), finalize(() => this.loading = false))
        .subscribe(authorization => {
          this.config.authorization = authorization;
          this.router.navigate(['/']);
        }, error => this.error = error);
    }
  }
}
