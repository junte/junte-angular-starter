import { NgModule } from '@angular/core';
import { SigninComponent } from './signin.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ErrorModule } from '../shared/error/module';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { SigninRoutingModule } from './signin-routing.module';
import { usersServiceProvider } from '../../services/users/users.provider';

@NgModule({
  declarations: [
    SigninComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    ErrorModule,
    TranslateModule,
    SigninRoutingModule
  ],
  providers: [
    usersServiceProvider
  ]
})

export class SigninModule {
}
