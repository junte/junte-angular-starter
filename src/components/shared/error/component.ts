import {Component, Input} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {
  ApplicationError, Error, FatalError,
  ForbiddenError,
  InvalidGrantError,
  NetworkError,
  NotFoundError
} from '../../../../projects/junte-angular/src/models/errors';

@Component({
  selector: 'app-error',
  templateUrl: './template.html',
  styleUrls: ['./style.scss']
})

export class ErrorComponent {

  private _error: Error;
  text: string;

  @Input() set error(error: Error) {
    let key: string;
    if (error instanceof NetworkError) {
      key = 'LABEL_NETWORK_ERROR';
    } else if (error instanceof FatalError) {
      key = 'LABEL_FATAL_ERROR';
    } else if (error instanceof ForbiddenError) {
      key = 'FORBIDDEN_ERROR';
    } else if (error instanceof NotFoundError) {
      key = null;
    } else if (error instanceof InvalidGrantError) {
      key = null;
    } else if (error instanceof ApplicationError) {
      key = null;
    } else {
      key = 'LABEL_UNKNOWN_ERROR';
    }

    if (!!key) {
      this.translate.get(key).subscribe(label => this.text = label);
    } else {
      this.text = error.reasons.map(r => r.message).join(', ');
    }
    this._error = error;
  }

  get error() {
    return this._error;
  }

  constructor(private translate: TranslateService) {

  }
}
