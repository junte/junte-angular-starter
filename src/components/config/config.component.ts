import {Component} from '@angular/core';
import {AppConfig} from 'app-config';
import {FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.scss']
})

export class ConfigComponent {

  developmentVersion = 'http://junte.it';

  configForm = this.fb.group({
    useMocks: [this.config.useMocks],
    mocksDelay: [this.config.mocksDelay],
    backendEndpoint: [this.config.backendEndpoint],
    language: [this.config.language]
  });

  constructor(public config: AppConfig,
              private fb: FormBuilder,
              private router: Router) {
  }

  onSubmit() {
    Object.assign(this.config, this.configForm.value);
    this.router.navigateByUrl('/');
  }
}
