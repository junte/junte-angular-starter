import { Component, OnInit } from '@angular/core';
import { MeManager } from '../../managers/me.manager';
import { filter } from 'rxjs/operators';
import { Me } from '../../models/me';

@Component({
  selector: 'app-me',
  templateUrl: './me.component.html',
  styleUrls: ['./me.component.scss']
})
export class MeComponent implements OnInit {

  private user: Me;

  constructor(public me: MeManager) {
  }

  ngOnInit() {
    this.me.user$.pipe(filter(user => !!user))
      .subscribe(user => this.user = user);
  }

}
