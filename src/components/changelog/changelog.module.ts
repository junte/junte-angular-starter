import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ChangelogRoutingModule} from './changelog-routing.module';
import {ChangelogComponent} from './changelog.component';
import {Angular7Component} from './angular7/angular7.component';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {DragDropModule} from '@angular/cdk/drag-drop';

@NgModule({
  declarations: [
    ChangelogComponent,
    Angular7Component
  ],
  imports: [
    CommonModule,
    ChangelogRoutingModule,
    ScrollingModule,
    DragDropModule
  ]
})
export class ChangelogModule {
}
