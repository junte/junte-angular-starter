import { Component } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from "@angular/cdk/drag-drop";

@Component({
  selector: 'app-angular7',
  templateUrl: './angular7.component.html',
  styleUrls: ['./angular7.component.scss']
})
export class Angular7Component{

  numbers: any[] = [];
  otherNumbers: any[] = [];

  constructor() {
    for (let i = 0; i < 100; i++) {
      this.numbers.push(i);
    }
  }

  singleDrop(event: CdkDragDrop<any[]>) {
    moveItemInArray(this.numbers, event.previousIndex, event.currentIndex)
  }

  multiDrop(event: CdkDragDrop<any[]>) {
    if(event.previousContainer !== event.container) {
      transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
    } else {
      moveItemInArray(this.numbers, event.previousIndex, event.currentIndex)
    }
  }

}
