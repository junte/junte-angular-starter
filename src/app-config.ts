import {Inject, Injectable} from '@angular/core';
import {Config} from '../projects/junte-angular/src/config';
import {BehaviorSubject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {DOCUMENT} from '@angular/common';
import {finalize} from 'rxjs/operators';

const APP_VERSION = '2.0.1';
const CHECK_VERSION_TIME = 300000;

@Injectable()
export class AppConfig extends Config {

  version$ = new BehaviorSubject<string>(APP_VERSION);

  set version(version: string) {
    this.version$.next(version);
  }

  get version() {
    return this.version$.getValue();
  }

  set backendEndpoint(backendEndpoint: string) {
    if (!!backendEndpoint) {
      localStorage.setItem('backendEndpoint', backendEndpoint);
    } else {
      localStorage.removeItem('backendEndpoint');
    }
  }

  get backendEndpoint(): string {
    return localStorage.backendEndpoint !== undefined
      ? localStorage.backendEndpoint : this.localMode ? 'http://localhost:4200' : 'http://server/';
  }

  private checker: any;

  constructor(@Inject(HttpClient) private http: HttpClient,
              @Inject(DOCUMENT) private document: Document) {
    super();
    this.checkVersion();
  }

  private checkVersion() {
    clearTimeout(this.checker);
    this.http.get('version.txt', {responseType: 'text'})
      .pipe(finalize(() => this.checker = setInterval(this.checkVersion.bind(this), CHECK_VERSION_TIME)))
      .subscribe(version => {
        console.log('check version');
        console.log('version:', version);
        if (version && version.trim() !== this.version) {
          console.log('new version detected = ' + version);
          this.version = version;
        }
      });
  }


}
