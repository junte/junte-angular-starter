export enum LoggingLevel {
  Debug,
  Info,
  Warning,
  Error,
  None
}
