import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AppConfig} from 'app-config';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()
export class AuthorizationGuard implements CanActivate {

  constructor(private config: AppConfig,
              private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.config.authorization$
      .pipe(map(authorization => {
        if (!authorization) {
          this.router.navigate(['/signin']);
        }
        return !!authorization;
      }));
  }
}
