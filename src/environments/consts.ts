export const EN_LANG = 'en';
export const RU_LANG = 'ru';
export const DE_LANG = 'de';
export const ES_LANG = 'es';
export const FR_LANG = 'fr';
export const IT_LANG = 'it';
export const PT_LANG = 'pt';
export const PLATFORM_DELAY = 500;
