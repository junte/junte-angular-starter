import { InjectionToken } from '@angular/core';
import { Me } from 'models/me';
import { Observable } from 'rxjs';

export interface IMeService {
  getMe(): Observable<Me>;

  heartbeat(): Observable<any>;
}

export let me_service = new InjectionToken('IMeService');
