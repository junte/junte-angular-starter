import { Injectable } from '@angular/core';
import { IMeService } from 'services/me/me.interface';
import { HttpService } from '../../../projects/junte-angular/src/services/http.service';
import { Me } from 'models/me';
import { Observable } from 'rxjs';

@Injectable()
export class MeService implements IMeService {

  constructor(private http: HttpService) {
  }

  getMe(): Observable<Me> {
    return this.http.get<Me>('me');
  }

  heartbeat(): Observable<any> {
    return this.http.post<any>('me/heartbeat');
  }
}
