import { Injectable } from '@angular/core';
import { IMeService } from './me.interface';
import { HttpMockService } from '../../../projects/junte-angular/src/services/http-mock.service';
import { Observable, of } from 'rxjs';
import { Me } from 'models/me';


@Injectable()
export class MeMockService implements IMeService {

  constructor(private http: HttpMockService) {
  }

  getMe(): Observable<Me> {
    return this.http.get('me/me.json');
  }

  heartbeat(): Observable<any> {
    return of(null);
  }
}
