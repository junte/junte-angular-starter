import {InjectionToken} from '@angular/core';
import {Observable} from 'rxjs';
import {Animal} from '../../models/animal';

export interface ITestService {
  get(): Observable<Animal>;
}

export let test_service = new InjectionToken('ITestService');
