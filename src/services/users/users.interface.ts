import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { Authorization } from '../../../projects/junte-angular/src/models/authorization';
import { LoginCredentials } from '../../models/login-credentials';

export interface IUsersService {
  login(credentials: LoginCredentials): Observable<Authorization>;
}

export let users_service = new InjectionToken('IUsersService');
