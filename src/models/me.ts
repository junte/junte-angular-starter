export interface Me {
  id: number;
  login: string;
  permissions: string[];
}
