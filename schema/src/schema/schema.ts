export interface SchemaOptions {
  name: string;
  appRoot: string;
  path: string;
  sourceDir: string;
  service: string;
  model: string;
  mock: string;
}
