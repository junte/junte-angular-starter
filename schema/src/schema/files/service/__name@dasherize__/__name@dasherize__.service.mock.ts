import { Injectable } from '@angular/core';
import { HttpMockService } from 'services/http-mock.service';
import { Observable } from 'rxjs';
import { I<%= classify(name) %>Service } from 'services/<%= dasherize(name) %>/<%= dasherize(name) %>.interface';
import { PagingResults } from 'models/paging-results';

@Injectable()
export class <%= classify(name) %>MockService implements I<%= classify(name) %>Service {

  constructor(private http: HttpMockService) {
  }

}
