import { AppConfig } from 'app-config';
import { HttpMockService } from 'services/http-mock.service';
import { HttpService } from 'services/http.service';
import { <%= underscore(name) %>_service } from 'services/<%= dasherize(name) %>/<%= dasherize(name) %>.interface';
import { <%= classify(name) %>Service } from 'services/<%= dasherize(name) %>/<%= dasherize(name) %>.service';
import { <%= classify(name) %>MockService } from 'services/<%= dasherize(name) %>/<%= dasherize(name) %>.service.mock';

export function <%= classify(name) %>ServiceFactory(httpService: HttpService, httpMockService: HttpMockService, config: AppConfig) {
    return config.useMocks ?
        new <%= classify(name) %>MockService(httpMockService) :
        new <%= classify(name) %>Service(httpService);
}

export let <%= classify(name) %>ServiceProvider = {
    provide: <%= underscore(name) %>_service,
    useFactory: <%= classify(name) %>ServiceFactory,
    deps: [HttpService, HttpMockService, AppConfig]
};
