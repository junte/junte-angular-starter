
# JunteAngular

  Common functional for implement it into other angular projects

## Installation

```
$ npm install junte-angular
```

## Uses
* import {HttpMockService} from 'junte-angular';
* import {JunteConfig} from 'junte-angular';
* import {
    Authorization,
    ApplicationError,
    FatalError,
    ForbiddenError,
    InvalidGrantError,
    NetworkError,
    NotFoundError
  } from 'junte-angular';

## License

[MIT](LICENSE)

## Versions

### 0.0.5 - 0.0.7
* refactor
* add config page
* add http-service
* add ObjectLink and PagingResults models
* add SignalService
* gulp version build

### 0.0.3

refactor

### 0.0.2

added
* http-mock service;
* autorization;
* JunteConfig;
* app error classes

