import { BehaviorSubject } from 'rxjs';
import { Authorization } from './models/authorization';

const DEFAULT_MOCKS_DELAY = 500;
const DEFAULT_LANGUAGE = 'en';
const AUTHORIZATION_KEY = 'Authorization';

export abstract class Config {

  localMode: boolean = (href => {
    // href = 'http://localhost';
    const regex = /(localhost|127.0.0.1)/ig;
    return regex.test(href);
  })(window.location.href);

  authorization$ = new BehaviorSubject<Authorization>((() => {
    if (!!localStorage[AUTHORIZATION_KEY]) {
      return JSON.parse(localStorage[AUTHORIZATION_KEY]) as Authorization;
    }

    return null;
  })());

  set authorization(authorization: Authorization) {
    if (!!authorization) {
      localStorage.setItem(AUTHORIZATION_KEY, JSON.stringify(authorization));
    } else {
      localStorage.removeItem(AUTHORIZATION_KEY);
    }

    this.authorization$.next(authorization);
  }

  get authorization() {
    return this.authorization$.getValue();
  }

  language$ = new BehaviorSubject<string>(!!localStorage.language ? localStorage.language : DEFAULT_LANGUAGE);

  set language(language: string) {
    localStorage.setItem('language', language);
    this.language$.next(language);
  }

  get language() {
    return this.language$.getValue();
  }

  backendEndpoint: string;
  mocksPath = '/assets/mocks';

  set useMocks(value: boolean) {
    localStorage.setItem('useMocks', value ? '1' : '');
  }

  get useMocks() {
    if (localStorage.useMocks !== undefined) {
      return localStorage.useMocks;
    }
    const href = window.location.href;
    return /use-mocks/i.test(href);
  }

  set mocksDelay(value: number) {
    localStorage.setItem('mocksDelay', `${value}`);
  }

  get mocksDelay() {
    if (localStorage.mocksDelay !== undefined) {
      return localStorage.mocksDelay;
    }
    return DEFAULT_MOCKS_DELAY;
  }

}
