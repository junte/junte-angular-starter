import {NgModule} from '@angular/core';
import {FieldTouchedHasErrorPipe} from './forms/field-has-error';

@NgModule({
  declarations: [FieldTouchedHasErrorPipe],
  exports: [FieldTouchedHasErrorPipe]
})
export class FormPipesModule {

}
