var gulp = require('gulp');
var debug = require('gulp-debug');
var iconfont = require('gulp-iconfont');
var consolidate = require('gulp-consolidate');
var rename = require('gulp-rename');
var iconFiles = "src/assets/icons/*.svg";
var runTimeStamp = Math.round(Date.now() / 1000);

var path = require('path');
var map = require('map-stream');
var dummyjson = require('dummy-json');
var extname = require('gulp-extname');
var partials = {};
var helpers = {};

let VERSION = null;

var shell = require('gulp-shell');
var args = require('yargs').argv;
var changeCase = require('change-case');


// iconfonts
const iconfonts = function iconsRender() {
  return gulp.src([iconFiles])
    .pipe(debug({title: 'iconfont:'}))
    .pipe(iconfont({
      fontName: 'icons',
      prependUnicode: true,
      formats: ['ttf', 'woff', 'svg', 'eot', 'woff2'],
      timestamp: runTimeStamp,
      normalize: true,
      fontHeight: 1001,
      appendCodepoints: true
    }))
    .on('glyphs', function (glyphs, options) {
      glyphs.forEach(function (glyph, idx, arr) {
        arr[idx].unicode[0] = glyph.unicode[0].charCodeAt(0).toString(16);
      });
      gulp.src('src/assets/styles/_icons-template.scss')
        .pipe(consolidate('lodash', {
          glyphs: glyphs,
          fontName: 'icons',
          fontPath: '../fonts/icons/',
          className: 'icon'
        }))
        .pipe(rename('_icons.scss'))
        .pipe(gulp.dest('src/assets/styles/'))
    })
    .pipe(gulp.dest('src/assets/fonts/icons'))
};


// mocks
const mockPartials = function () {
  return gulp.src(['src/mocks/objects/*.jhbs'])
    .pipe(debug())
    .pipe(map(function (file, cb) {
      var attrName = path.basename(file.path, '.jhbs');
      partials[attrName] = file.contents.toString();
      cb();
    }));
};

const mockHelpers = function () {
  return gulp.src(['src/mocks/helpers/*.json'])
    .pipe(debug())
    .pipe(map(function (file, cb) {
      var attrName = path.basename(file.path, '.json');
      helpers[attrName] = function () {
        return dummyjson.utils.randomArrayItem(JSON.parse(file.contents.toString()))
      };
      cb();
    }));
};

const mockObjects = gulp.series(gulp.parallel(mockPartials, mockHelpers), function objectsRender(done) {
  gulp.src(['src/mocks/objects/*.jhbs'])
    .pipe(debug())
    .pipe(map(function (file, cb) {
      file.contents = new Buffer(dummyjson.parse(file.contents.toString(),
        {partials: partials, helpers: helpers}));
      return cb(null, file);
    }))
    .pipe(extname('.json'))
    .pipe(gulp.dest('src/assets/mocks/objects/'));

  done();
});

const mockServices = gulp.series(gulp.parallel(mockPartials, mockHelpers), function serversRender(done) {
  gulp.src(['src/mocks/services/**/*.jhbs'])
    .pipe(debug())
    .pipe(map(function (file, cb) {
      file.contents = new Buffer(dummyjson.parse(file.contents.toString(),
        {partials: partials, helpers: helpers}));
      return cb(null, file);
    }))
    .pipe(extname('.json'))
    .pipe(gulp.dest('src/assets/mocks/services'));

  done();
});

const mockImages = function imagesRender(done) {
  gulp.src(['src/mocks/images/*'])
    .pipe(debug())
    .pipe(gulp.dest('src/assets/mocks/images/'));
  done();
};

const mocks = gulp.series(mockServices, mockImages);


// version
const versionUpgrade =  function versionUpgrade() {
  return gulp.src(['src/version.txt'])
    .pipe(map(function (file, cb) {
      var contents = file.contents.toString();
      VERSION = contents.split('.');

      var build = Number(VERSION.pop());
      var minor = Number(VERSION.pop());
      var major = Number(VERSION.pop());

      if (args.major) {
        major += 1;
        minor = 0;
        build = 0;
      }
      if (args.minor) {
        minor += 1;
        build = 0;
      }
      if (args.build) {
        build += 1;
      }

      VERSION.push(major.toString());
      VERSION.push(minor.toString());
      VERSION.push(build.toString());

      file.contents = new Buffer(VERSION.join('.'));
      return cb(null, file);
    }))
    .pipe(gulp.dest('src/'));
};

const version = gulp.series(versionUpgrade, function configUpgrade(done) {
  gulp.src(['src/app-config.ts'])
    .pipe(map(function (file, cb) {
      var contents = file.contents.toString();
      var pattern = /APP\_VERSION\s=\s\'[0-9]+\.[0-9]+\.[0-9]+\'/g;
      file.contents = new Buffer(contents.replace(pattern, "APP_VERSION = '" + VERSION.join('.') + "'"));
      return cb(null, file);
    }))
    .pipe(gulp.dest('src/')); // TODO: app-config can't replace yourself
  done();
});


// schematics
const model = gulp.series(
  shell.task([`ng g schema:schema --m=${args.name}`]),
  shell.task([`git add ${args.name}.ts`], {cwd: 'src/models'})
);

const service = gulp.series(
  shell.task([`ng g schema:schema --s=${args.name}`]),
  shell.task([`git add ${args.name}.ts`], {cwd: 'src/services'})
);

const mockHelper = gulp.series(
  shell.task([`ng g schema:schema --mock=h --name=${args.name}`]),
  shell.task([`git add ${changeCase.camelCase(args.name)}.json`], {cwd: 'src/mocks/helpers'})
);

const mockObject = gulp.series(
  shell.task([`ng g schema:schema --mock=o --name=${args.name}`]),
  shell.task([`git add ${changeCase.camelCase(args.name)}.jhbs`], {cwd: 'src/mocks/objects'})
);

const component = gulp.series(
  shell.task([`ng g component ${args.name} ${args.skip ? '--skip-import' : ''}`]),
  shell.task([`git add .`], {cwd: 'src/components'})
);

const componentModule = gulp.series(
  shell.task([`ng g module ${args.name} ${args.r ? '--routing' : ''}`]),
  shell.task([`git add .`], {cwd: 'src/components'})
);


module.exports = {
  iconfonts,
  mockObjects,
  mockServices,
  mockImages,
  mocks,
  version,
  model,
  service,
  mockHelper,
  mockObject,
  component,
  module
};
